## Compiling

Install `ghc` and `cabal`. Then run

    cabal update
    cabal sandbox init
    cabal install --dependencies-only
    cabal build

This will generate the binary `boomange` in `dist/build/boomange/`.

# Synopsis

  boomange [OPTIONS...]

# Description

In order to allow a unified and customized bookmarks file among different browsers, boomange generates a HTML file with an user's bookmarks, allowing customisations in the form of header and footer files.

# Features

  - HTML generation, with custom header, footer and css style.
  - Bookmarks are stored in human-readable text files.

# Options

  **`-a, --add <location> <URI>`**

  Add a bookmark to file with the given id.
  The depths of <location> should be separated with '/'s.
  The id are the left-side values inside the "watch" section in the config file.

  Example: boomange --add 'id/desired/path' 'http://en.wikipedia.org/'

  **`-c, --config <file>`**

  Use `file` as configuration file instead of the default one (see below).

  **`-h, --help`**

  Show help.

  **`-s, --status`**

  Output configuration files info and exit.

  **`-v, --version`**

  Print version and exit.

# Files

boomange stores its configuration files in one of these directories by default:

  - $XDG_CONFIG_HOME/boomange

  - $HOME/.config/boomange

  - Haskell's System.Directory.getAppUserDataDirectory boomange

using the first solvable (i.e., the directory doesn't have to exist, only the variable). If the directory does not exist, a sample configuration will be installed.

When executed, boomange searches for the config file inside the configuration folder, which uses descrilo syntax. It contains 2 sections:


  - config -- configuration of what boomange should do. The possible values inside are:

    - output -- where the HTML should be placed after generation

    - headerFile -- file that will be prepended to output

    - footerFile -- file that will be appended to output

  - watch -- all files that should be read for bookmarks. The left part of every value is irrelevant, and the right part indicates the file. Each file should use simtreelo syntax, and all leaves are used as  URI for their parents, which means that a node with a leaf as child should have no other children.

# Bugs

No known bugs.

# Author

Marcelo Garlet Millani (marcelogmillani@gmail.com)
