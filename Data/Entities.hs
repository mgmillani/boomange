{-
        Copyright 2013,2014 Marcelo Millani

        This file is part of boomange.

    boomange is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    boomange is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with boomange.  If not, see <http://www.gnu.org/licenses/>
-}
module Data.Entities where

data Config =
        Config
        { headerFile ::  [Char]
        , footerFile ::  [Char]
        , outputFile ::  [Char]
        , watch      :: [[Char]]
        }

data Bookmark = Folder{name :: [Char], children :: [Bookmark]} | Url{name :: [Char], url :: [Char]} deriving (Show)

defaultConfig =
        Config
        { headerFile = "html/header.html"
        , footerFile = "html/footer.html"
        , outputFile = "bookmarks.html"
        , watch      = []
        }

