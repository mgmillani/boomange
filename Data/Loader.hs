{-
	Copyright 2013,2014 Marcelo Millani
	
	This file is part of boomange.

    boomange is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    boomange is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with boomange.  If not, see <http://www.gnu.org/licenses/>
-}
module Data.Loader where

import Data.List

import Data.DescriLo

import qualified Data.Simtreelo as Simtreelo
import Data.Tree
import Data.Builder
import Data.Entities hiding (name)

createConfig description = createConfig' (values description) defaultConfig
createConfig' [] config = config
createConfig' (h:r) config = 
  case h of
    ("output",outp) -> createConfig' r config{outputFile = outp}
    ("header",head) -> createConfig' r config{headerFile = head}
    ("footer",foot) -> createConfig' r config{footerFile = foot}
    _               -> createConfig' r config

loadConfig cFile = do
  description <- loadDescriptionFile cFile "config"
  let Just c = find (\x -> (name x) == "config") description
  let config = createConfig c
  let Just w = find (\x -> (name x) == "watch") description
  return $ (createConfig c){watch = map (\(l,r) -> r) (values w)}

-- | builds the bookmark tree	
loadBookmarks [] = do return []
loadBookmarks (h:r) = do
  books <- Simtreelo.loadFile h
  let here = case books of
        Right forest -> -- forest
          if null $ filter (\x -> null $ subForest x) forest then forest
          else [makeError "empty tree" h]
        Left error -> [makeError error h]
  rest <- loadBookmarks r
  return $ (map (buildBookmarks h) here) ++ rest
